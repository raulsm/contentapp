#!/usr/bin/env python

import webapp

class contentApp(webapp.webApp):

    #Diccionario de contenido, lo declaramos e inicializamos en el constructor
    content = {"/": "Pagina principal","/page": "Pagina 1"}

    def parse(self, request):

        #Me devolvera el recurso solicitado
        return request.split(" ", 2)[1]

    def process(self, resourceName):

        if resourceName in self.content:
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[resourceName] + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Not Found"


        return httpCode, htmlBody

if __name__ == "__main__":

    testWebApp = contentApp("localhost", 1235)