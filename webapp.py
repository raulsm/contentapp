#!/usr/bin/env python

import socket


class webApp:

    def parse(self, request):
        """Parseo la peticion extrayendo la info"""

        print("parse: No hay nada que implementar")
        return None

    def process(self, parsed_request):
        """Procesa los datos de la peticion
        Devuelve el codigo HTTP de la respuesta
        y una pagina HTML
        """

        print("process: No hay mada que procesar")
        return "200 OK", "<html><body>Hola mundo!</body></html>"

    def __init__(self, hostname, port):
        """Iniciar nuestra aplicacion web"""

        # Configracion del socket
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))
        mySocket.listen(5)

        while True:
            print("Esperando alguna conexion...")
            connectionSocket, addr = mySocket.accept()
            print("Conexion recibida de: " + str(addr))
            recibido = connectionSocket.recv(2048)
            print("El crudo de bytes: ", recibido)

            # Manejar/parsear peticion, los recursos
            parsed_request = self.parse(recibido.decode("utf-8"))

            # Proceso la peticion y creo la respuesta
            return_code, html_respuesta = self.process(parsed_request)

            # Enviar respuesta
            respuesta = "HTTP/1.1 " + return_code + "\r\n\r\n" \
                        + html_respuesta + "\r\n"
            connectionSocket.send(respuesta.encode("utf-8"))
            connectionSocket.close()


if __name__ == '__main__':
    my_webapp = webApp("", 1235)

